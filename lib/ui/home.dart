import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/basic.dart';

class MyCalc extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return new MYhomepage();
      }
    
    }
    
    class MYhomepage extends State<MyCalc> {

        final  TextEditingController _firstnumber =new TextEditingController();
          final TextEditingController _secondnumber =  new TextEditingController();
          double result =0.0;


  
  
void _add()
{
  setState(() {
    double num1 =double.parse(_firstnumber.text);
    double num2 =double.parse(_secondnumber.text);
    
       result=num1+num2;

  });
  

}
void _subtract()
{ 
   setState(() {
    double num1 =double.parse(_firstnumber.text);
    double num2 =double.parse(_secondnumber.text);
    
       result=num1-num2;

  });
}
void _multiply()
{
   setState(() {
    double num1 =double.parse(_firstnumber.text);
    double num2 =double.parse(_secondnumber.text);
    
       result=num1*num2;

  });
}
void _divide()
{
   setState(() {
    double num1 =double.parse(_firstnumber.text);
    double num2 =double.parse(_secondnumber.text);
    if(num1 != 0 && num2 != 0)
    {
       result=num1/num2;

    }
    else{
      result=0;

    }


  });
}

@override
  Widget build(BuildContext context) {
    
    // TODO: implement build
    return new Scaffold(
      appBar: new AppBar(
        title: new Text('CALCULTOR PAGE'),
        centerTitle: true,
        backgroundColor: Colors.pinkAccent,
      ),
      body: new Container(
        alignment: Alignment.center,
        child: new ListView(
          padding: const EdgeInsets.all(2.0),
          children: <Widget>[
            new Container(
              margin: const EdgeInsets.all(3.0),
              height: 50,
              width: 290,
              color: Colors.white24,
              child: new Column(
                children: <Widget>[
                  Text('PLEASE FILL DETAILS',
                  style: TextStyle(height: 2, fontSize: 25)),
                  
                ],
              ),
            ),
            new Container(
              margin: const EdgeInsets.all(3.0),
              height: 245,
              width: 290,
              color: Colors.grey.shade300,
              child: new Column(
                children: <Widget>[
                  new TextField(
                    controller: _firstnumber,
                    keyboardType: TextInputType.text,
                    decoration: new InputDecoration(
                      labelText: 'Enter 1st number',
                      icon: new Icon(Icons.confirmation_number)
                    ),
                  ),

                  new TextField(
                    controller: _secondnumber,
                    keyboardType: TextInputType.text,
                    decoration: new InputDecoration(
                      labelText: 'Enter 2nd number',

                      icon: new Icon(Icons.confirmation_number)
                    ),
                  ),
new Padding(padding:  new EdgeInsets.all(10.6)),
    Row(children: <Widget>[
                    Expanded(
                      child: RaisedButton(
                         onPressed: ()=> _add(),
            color: Colors.pinkAccent,
            child: new Text('Add'),
            textColor: Colors.white,
                      ),
                    ),
                    Expanded(
                      child: RaisedButton(
                       onPressed: ()=> _subtract(),
            color: Colors.pinkAccent,
            child: new Text('Subtract'),
            textColor: Colors.white,
                      ),
                    ),
                    Expanded(
                      child: RaisedButton(
                        onPressed: ()=>_multiply(),
            color: Colors.pinkAccent,
            child: new Text('Multiply'),
            textColor: Colors.white,
                      ),
                    ),
                    Expanded(
                      child: RaisedButton(
                        onPressed: ()=> _divide(),
            color: Colors.pinkAccent,
            child: new Text('Divide'),
            textColor: Colors.white,
                      ),
                    )
                  ])


   


 
             

                ],
              ),
            ),
            new Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                new Text("Result is: $result ",
                style: new TextStyle(
                  color: Colors.blueAccent ,
                  fontWeight: FontWeight.w500,
                  fontStyle: FontStyle.italic,
                  fontSize: 19.9
                ),

                )
              ],
            )
          ],
        )
      ),
    );
  }

}